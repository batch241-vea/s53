import { useState, useEffect } from 'react';

import { Form, Button} from 'react-bootstrap';

import { useNavigate } from 'react-router-dom';

export default function Login() {

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
   	const [isActive, setIsActive] = useState(false);

    //hook returns a function that lets you navigate to components
    const navigate = useNavigate();

   	function authenticate(e){
   		e.preventDefault()

        // set the email of the authenticated user in local storage
        // localStorage.setItem('propertyName', value)
        localStorage.setItem('email', email);

   		setEmail('');
        setPassword('');
        navigate('/');

        alert('You are now logged in!')
   	}

   	useEffect(()=> {
        if(email !== '' && password !== ''){
            setIsActive(true);
        } else{
            setIsActive(false);
        }
    }, [email, password]);

	return (
		
        <Form onSubmit={(e) => authenticate(e)}>
        <h1>Login</h1><br/>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value = {email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value = {password}
                    onChange = {e => setPassword(e.target.value)}
                    required
                />
            </Form.Group><br/>

            {isActive ?
            <Button variant="success" type="submit" id="submitBtn">
                Submit 
            </Button>
            :
            <Button variant="success" type="submit" id="submitBtn" disabled>
                Submit 
            </Button>
            }
        </Form>
    )

}