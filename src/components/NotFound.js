import {Button, Row, Col} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

export default function Error() {
return (
    <Row>
    	<Col className="p-5">
            <h1>Page Not Found</h1>
            <p>Go back to the homepage.</p>
            <Button variant="primary" as={NavLink} to="/">Home</Button>
        </Col>
    </Row>
	)
}