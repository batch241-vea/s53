import './App.css';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './components/NotFound';

import { Container } from 'react-bootstrap';

function App() {
  return (
    /*Fragments - common pattern in React.js for a component to return multiple elements*/
    <>
    {/*Initializes that dynamic routing will be involved*/}
      <Router>  
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/courses" element={<Courses/>}/>
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="*" element={<NotFound/>} />

          </Routes>
        </Container>
      </Router> 
    </>
  );
}

export default App;
